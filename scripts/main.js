'use strict';

import {app} from './app.js';

//let apiUrl = 'https://algorit.io/etc/bsale/api-php';
let apiUrl = 'http://local.local/api';

/**
* Punto de inicio del programa. Genera una instancia de la clase app.
*/
window.onload = async function (){
	let App = new app(
		apiUrl,
		{
			buscador: '#buscador',
			carrito: '#carrito',
			contenido: '#contenido',
		}
	);
}