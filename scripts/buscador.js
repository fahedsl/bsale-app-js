'use strict';

import {ui} from './util/ui.js';

/**
Módulo que exporta la clase buscador.
@module buscador
*/

/**
Contiene el buscador de productos. Al iniciar puebla el contenedor del buscador principal.
@class
@name buscador
@param {catalogo} catalogo -  - Catálogo de la app. Relacionado para permitir llamada de funciones;
@param {object-HTML} contenedor - Contenedor HTML del buscador.
@returns none
@example
let divBuscador = document.querySelector(contenedores.buscador);
let carrito = new carrito(divCarrito, divContenido);
let catalogo = await new catalogo(api, carrito, divContenido);
let buscador = new buscador(catalogo, divBuscador);
*/
function buscador (catalogo, contenedor){
	this.catalogo = catalogo;
	this.contenedor = contenedor;

	ui.poblar(this.contenedor, this.buscadorUI(this.catalogo));
}

/**
Se vincula a un método de la clase UI, para generar el HTML del buscador.
@function
@returns {object-HTML}
*/
buscador.prototype.buscadorUI = ui.buscador.buscador;

export {buscador};