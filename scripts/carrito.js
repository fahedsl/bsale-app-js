'use strict';

import {ui} from './util/ui.js';

/**
Módulo que exporta la clase carrito.
@module carrito
*/

/**
Contiene el carrito de compras. Almacena los productos seleccionados por el usuario.
Al iniciar puebla el contenedor de icono.
@class
@name carrito
@param {object-HTML} contenedorIcono - div contenedor del icono del carrito.
@param {object-HTML} contenedorCarrito - div contenedor del carritod e compras.
@returns none
@example
let carrito = new carrito(divCarrito, divContenido);
let catalogo = await new catalogo(api, carrito, divContenido);
let buscador = new buscador(catalogo, divBuscador);
carrito.catalogo = catalogo;
*/
function carrito (contenedorIcono, contenedorCarrito){

	/**
	@property {object-HTML} contenedorIcono - Contenedor HTML del icono de carrito.
	@property {object-HTML} contenedorCarrito - Contenedor HTML del carrito.
	@property {catalogo} catalogo - Catálogo de la app. Relacionado para permitir llamad de funciones;
	@property {object[]} carritoLista - Lista de productos agregados;
	@property {bool} carritoLleno - Indica si el carrito tiene productos;
	@property {bool} carritoMostrado - Indica si el HTML del carrito está siendo mostrado;
	@property {bool} carritoVendido - Indica si el contenido del carrito acaba de ser vendido;
	*/
	this.contenedorIcono = contenedorIcono;
	this.contenedorCarrito = contenedorCarrito;
	this.catalogo = null;
	this.carritoLista = [];
	this.carritoLleno = false;
	this.carritoMostrado = false;
	this.carritoVendido = false;

	ui.poblar(this.contenedorIcono, this.iconoUIConstruir());
}

/**
Agrega productos al carrito.
@function
@param {object} producto - Producto que se agrega.
@param {int} [cantidad] - Cantidad que se agrega.
@returns none
@example
let producto = {id: 5, name: "ENERGETICA MR BIG"};
carrito.productoAgregar(producto);
@example
let producto = {id: 5, name: "ENERGETICA MR BIG"};
carrito.productoAgregar(producto, 5);
*/
carrito.prototype.productoAgregar = function(producto, cantidad = 1){
	let listaTamano = this.carritoLista.length;
	
	if(listaTamano > 0){
		let encontrado = false;
		for(let i = 0; i < listaTamano; i++){
			if(this.carritoLista[i].id === producto.id){
				this.carritoLista[i].cantidad++;
				encontrado = true;
				break;
			}
		}
		if(!encontrado){
			producto.cantidad = cantidad;
			this.carritoLista.push(producto);
		}
	}
	else{
		producto.cantidad = cantidad;
		this.carritoLista.push(producto);
	}

	if(this.carritoLista.length > 0){
		this.carritoLleno = true;
	}
	else{
		this.carritoLleno = false;
	}

	ui.poblar(this.contenedorIcono, this.iconoUIConstruir());
}

/**
Retira productos del carrito.
@function
@param {object} producto - Producto que se agrega.
@param {int} [cantidad] - Cantidad que se agrega.
@returns none
@example
let producto = {id: 5, name: "ENERGETICA MR BIG"};
carrito.productoQuitar(producto);
@example
let producto = {id: 5, name: "ENERGETICA MR BIG"};
carrito.productoQuitar(producto, 5);
*/
carrito.prototype.productoQuitar = function(producto, cantidad = 1){
	let listaTamano = this.carritoLista.length;
	
	if(listaTamano > 0){
		let encontrado = false;
		for(let i = 0; i < listaTamano; i++){
			if(this.carritoLista[i].id === producto.id){
				this.carritoLista[i].cantidad--;
				encontrado = true;

				if(producto.cantidad == 0){
					
					function condicion(valor) { 
						return valor.id !== producto.id; 
					} 
		
					this.carritoLista = this.carritoLista.filter(condicion);
				}

				break;
			}
		}
	}

	if(this.carritoLista.length > 0){
		this.carritoLleno = true;
	}
	else{
		this.carritoLleno = false;
	}

	this.carritoUIMostrar();
}

/**
Genera el HTML de un producto.
@function
@param {object} producto
@returns {object-HTML}
@example
let producto = {id: 5, name: "ENERGETICA MR BIG"};
carrito.productoUIConstruir(producto);
*/
carrito.prototype.productoUIConstruir = function(datos){
	let retorno = this.productoUI(datos);
	return retorno;
}

/**
Inserta el HTML del grupo de productos del carrito de compras en el contenedor HTML.
@function
@returns none
*/
carrito.prototype.carritoUIMostrar = function(){
	let carritoUI = this.carritoUIConstruir();
	ui.poblar(this.contenedorCarrito, carritoUI);
	this.carritoMostrado = true;

	ui.poblar(this.contenedorIcono, this.iconoUIConstruir());
}

/**
Repone el HTML del catalogo en el contenedor HTML.
@function
@returns none
*/
carrito.prototype.carritoUIOcultar = function(){
	ui.poblar(this.contenedorCarrito, this.catalogo.catalogoUIConstruir(this.catalogo.productosDiv, this.catalogo.filtrosDiv));
	this.carritoMostrado = false;

	ui.poblar(this.contenedorIcono, this.iconoUIConstruir());
}

/**
Genera el HTML que contiene los productos. Incluye la creación del HTML de los productos.
@function
@returns {object-HTML}
*/
carrito.prototype.carritoUIConstruir = function(){
	let productos = document.createElement('div');
	if(this.carritoLista.length > 0){
		for(let i = 0; i < this.carritoLista.length; i++){
			if(this.carritoLista[i].filtrado === false){
				let tarjeta = this.productoUIConstruir(this.carritoLista[i]);
				productos.appendChild(tarjeta);
			}
		}
	}
	else{
		let tarjeta = this.vacioUI();
		productos.appendChild(tarjeta)
	}

	let retorno = this.carritoUI(productos);

	return retorno;
}

/**
Genera el HTML del icono.
@function
@returns {object-HTML}
*/
carrito.prototype.iconoUIConstruir = function(){
	let retorno = this.iconoUI(this.carritoLista.length);
	return retorno;
}

/**
Realiza la "venta" de los productos del carrito. Se ha puesto unicamente acciones de muestra.
@function
@returns {object-HTML}
*/
carrito.prototype.carritoVender = function(){
	this.carritoLista = [];
	this.carritoLleno = false;
	this.carritoVendido = true;
	this.carritoUIMostrar();
	this.carritoVendido = false;
}

/**
Se vincula a un método de la clase UI, para generar el HTML de carrito con productos.
@function
@returns {object-HTML}
*/
carrito.prototype.carritoUI = ui.carrito.carrito;

/**
Se vincula a un método de la clase UI, para generar el HTML de carrito vacio.
@function
@returns {object-HTML}
*/
carrito.prototype.vacioUI = ui.carrito.vacio;

/**
Se vincula a un método de la clase UI, para generar el HTML de un producto.
@function
@returns {object-HTML}
*/
carrito.prototype.productoUI = ui.carrito.producto;

/**
Se vincula a un método de la clase UI, para generar el HTML del icono.
@function
@returns {object-HTML}
*/
carrito.prototype.iconoUI = ui.carrito.icono;

export {carrito};