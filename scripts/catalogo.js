'use strict';

import {ui} from './util/ui.js';
import {urls} from './util/urls.js';
import {ajax} from './util/ajax.js';

/**
* Módulo que exporta la clase catálogo.
* @module catalogo
*/

/**
Contiene el catálogo de productos. Almacena la lista de productos disponibles.
Al iniciar puebla el contenedor principal.
@class
@name catalogo
@param {string} apiUrl - Url de root del API de donde se obtendran los datos.
@param {carrito} carrito - Carrito de compra de la app. Relacionado para permitir llamada de funciones;
@param {object-HTML} contenedor - Contenedor HTML del catalogo.
@returns none
@example
let apiUrl = 'http://local.local/api';
let divContenido = document.querySelector(contenedores.contenido);
let carrito = new carrito(divCarrito, divContenido);
let catalogo = await new catalogo(api, carrito, divContenido);
*/
function catalogo(apiUrl, carrito, contenedor){
	/**
	@property {string} apiUrl - Url de root del API de donde se obtendran los datos.
	@property {carrito} carrito - Carrito de compra de la app. Relacionado para permitir llamad de funciones.
	@property {object[]} productosLista - Lista de productos del catálogo.
	@property {object[]} filtrosLista - Lista de filtros del catálogo.
	@property {object-HTML} contenedor - Contenedor HTML del catálogo.
	@property {object-HTML} productosDiv - Contenedor HTML del los productos.
	@property {object-HTML} filtrosDiv - Contenedor HTML del los filtros.
	@property {object-HTML} catalogoDiv - Contenedor HTML del catálogo.
	*/
	this.apiUrl = apiUrl;
	this.carrito = carrito;
	this.contenedor = contenedor;

	this.productosLista = [];
	this.filtrosLista = [];

	this.productosDiv = null;
	this.filtrosDiv = null;
	this.catalogoDiv = null;

	this.catalogoCrear();
}

/**
Asíncrono. Inicia el catálogo. Al inicio puebla el HTML contenedor.
@function
@returns none
*/
catalogo.prototype.catalogoCrear = async function(){
	this.productosLista = await this.productosObtener();
	this.filtrosLista = await this.filtrosObtener();

	this.productosDiv = this.productosUIConstruir();
	this.filtrosDiv = this.filtrosUIConstruir();
	this.catalogoDiv = this.catalogoUIConstruir(this.productosDiv, this.filtrosDiv);
	
	ui.poblar(this.contenedor, this.catalogoDiv);
}

/**
Genera el HTML del catálogo.
@function
@param {object-HTML} productosDiv
@param {object-HTML} filtrosDiv
@returns {object-HTML}
@example
let productosDiv = productosUIConstruir();
let filtrosDiv = filtrosUIConstruir();
let catalogoDiv = catalogoUIConstruir(productosDiv, filtrosDiv);
*/
catalogo.prototype.catalogoUIConstruir = function(productosDiv, filtrosDiv){
	let retorno = this.catalogoUI(productosDiv, filtrosDiv);
	return retorno;
}

/**
Asíncrono. Consulta la base de datos por la lista de productos (con filtro) y genera el HTML correspondiente.
@function
@param {string} busqueda - Texto que se usará para la busqueda de productos.
@returns none
*/
catalogo.prototype.productosBuscar = async function(busqueda=null){
	this.productosLista = await this.productosObtener(busqueda);
	this.productosDiv = this.productosUIConstruir();
	this.catalogoDiv = this.catalogoUI(this.productosDiv, this.filtrosDiv);

	ui.poblar(this.contenedor, this.catalogoDiv);
}

/**
Asíncrono. Consulta la base de datos por la lista de productos.
@function
@param {string} busqueda - Texto que se usará para la busqueda de productos.
@returns {object}
*/
catalogo.prototype.productosObtener = async function(busqueda=null){
	let url = null;
	if(busqueda === '' || busqueda === null){
		url = urls.urlCrear(this.apiUrl);
	}
	else{
		url = urls.urlEntradaAParametro(this.apiUrl, 'busqueda', busqueda);
	}

	let datos = await ajax.get(url);
	datos = JSON.parse(datos);
	for(let i = 0; i < datos.length; i++){
		datos[i].filtrado = false;
	}

	return datos;
}

/**
Genera el HTML de los productos.
@function
@returns {object-HTML}
@example
let productosDiv = productosUIConstruir();
*/
catalogo.prototype.productosUIConstruir = function(){
	let retorno = document.createElement('div');
	if(this.productosLista.length > 0){
		for(let i = 0; i < this.productosLista.length; i++){
			if(this.productosLista[i].filtrado === false){
				let tarjeta = this.productoUI(this.productosLista[i]);
				retorno.appendChild(tarjeta);
			}
		}
	}
	else{
		retorno.appendChild(this.vacioUI());
	}

	return retorno;
}

/**
Asíncrono. Consulta la base de datos por la lista de filtros.
@function
@returns {object}
*/
catalogo.prototype.filtrosObtener = async function(){
	let url = urls.urlCrear(this.apiUrl + '/categoria');
	let retorno = await ajax.get(url);
	retorno = JSON.parse(retorno)
	for(let i = 0; i < retorno.length; i++){
		retorno[i].activo = true;
	}
	return retorno;
}

/**
Genera el HTML de los filtros.
@function
@returns {object-HTML}
@example
let filtrosDiv = filtrosUIConstruir();
*/
catalogo.prototype.filtrosUIConstruir = function(){
	let retorno = document.createElement('div')

	for(let i = 0; i < this.filtrosLista.length; i++){
		let tarjeta = this.filtroUI(this, i);
		retorno.appendChild(tarjeta);
	}

	return retorno;
}

/**
Regenera HTML del catálogo de acuerdo a los filtros activos.
@function
@param {object-HTML} valor - Campo (checkbox) que se ha modificado.
@return none
*/
catalogo.prototype.actualizarFiltro = function(valor){
	for( let i = 0; i < this.filtrosLista.length; i++){ 
		if ( this.filtrosLista[i].id == valor.value) {
			if(valor.checked === true){
				this.filtrosLista[i].activo = true;
			}
			else{
				this.filtrosLista[i].activo = false;
			}
			break;
		}
	}

	for( let i = 0; i < this.productosLista.length; i++){
		if(this.productosLista[i].cid == valor.value){
			if(valor.checked === true){
				this.productosLista[i].filtrado = false;
			}
			else{
				this.productosLista[i].filtrado = true;
			}
		}
	}

	this.productosDiv = this.productosUIConstruir();	
	this.catalogoDiv = this.catalogoUIConstruir(this.productosDiv, this.filtrosDiv);
	ui.poblar(this.contenedor, this.catalogoDiv);
}

/**
Se vincula a un método de la clase UI, para generar el HTML del catalogo.
@function
@returns {object-HTML}
*/
catalogo.prototype.catalogoUI = ui.catalogo.catalogo;

/**
Se vincula a un método de la clase UI, para generar el HTML del producto.
@function
@returns {object-HTML}
*/
catalogo.prototype.productoUI = ui.catalogo.producto;

/**
Se vincula a un método de la clase UI, para generar el HTML del catálogo vacío.
@function
@returns {object-HTML}
*/
catalogo.prototype.vacioUI = ui.catalogo.vacio;

/**
Se vincula a un método de la clase UI, para generar el HTML del filtro.
@function
@returns {object-HTML}
*/
catalogo.prototype.filtroUI = ui.catalogo.filtro;

export {catalogo};