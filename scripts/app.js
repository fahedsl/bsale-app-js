'use strict';

import {buscador} from './buscador.js';
import {catalogo} from './catalogo.js';
import {carrito} from './carrito.js';

/**
Módulo que exporta la clase app.
@module app
*/

/**
Crea la clase inicial. Esta clase contiene instancias de carrito, buscador, y catalogo, en las que opera el resto del programa.
@class
@name app
@param {string} api - Url de root del API de donde se obtendran los datos.
@param {object.Dict<string>} contenedores - Lista de selectores de contenedores del HTML generado. Deben ser selectores CSS.
@returns none
@example
let App = new app(
	apiUrl,
	{
		buscador: '#buscador',
		carrito: '#carrito',
		contenido: '#contenido',
	}
);
*/
function app(api, contenedores){
	this.api = api;
	this.divBuscador = document.querySelector(contenedores.buscador);
	this.divCarrito = document.querySelector(contenedores.carrito);
	this.divContenido = document.querySelector(contenedores.contenido);

	this.iniciar();
}

/**
Inicia las clases carrito, buscador, y catalogo, en las que opera el resto del programa.
@instance
@function
@name iniciar
@memberof app
@returns none
@example
this.iniciar();
*/
app.prototype.iniciar = async function(){
	this.carrito = new carrito(this.divCarrito, this.divContenido);
	this.catalogo = await new catalogo(this.api, this.carrito, this.divContenido);
	this.buscador = new buscador(this.catalogo, this.divBuscador);
	this.carrito.catalogo = this.catalogo;
}

export {app};