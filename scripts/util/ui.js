'use strict';

/**
Contiene las definiciones de los HTML de los objetos. Módulo que exporta la clase ui.
@class ui
*/
let ui = {
	/**
	Coloca un HTML dentro de otro, reemplazando el contenido previo.
	@function
	@memberof ui
	@param {object-HTML} destino - HTML exterior.
	@param {object-HTML} contenido - HTML interior.
	@returns {object-HTML}
	*/
	poblar: function (destino, contenido){
		destino.innerHTML = '';
		destino.appendChild(contenido);
	},

	buscador: {
		/**
		Genera HTML del buscador.
		@function
		@memberof ui
		@param {catalogo} catalogo - Catálogo de la app.
		@returns {object-HTML}
		*/
		buscador: function(catalogo){
			let form = document.createElement('form');
			let busqueda = document.createElement('input');
			let boton = document.createElement('input');
		
			form.appendChild(busqueda);
			form.appendChild(boton);
		
			form.action = '';
			form.method = 'get';
		
			busqueda.type = ('search');
			busqueda.placeholder = ('Tienda');
		
			boton.type = ('button');
			boton.classList.add('lupa');
		
			form.addEventListener(
				'submit',
				function(evento=null){
					if(evento !== null){
						evento.preventDefault();
					}
					catalogo.productosBuscar(busqueda.value);
				}
			);
			boton.addEventListener(
				'click',
				function(evento=null){
					if(evento !== null){
						evento.preventDefault();
					}
					catalogo.productosBuscar(busqueda.value);
				}
			);
		
			return form;
		}
	},

	carrito: {
		carrito: function(productos){
			let contenedor = document.createElement('div');
			let titulo = document.createElement('h2');

			contenedor.appendChild(titulo);
			contenedor.appendChild(productos);

			contenedor.classList.add('carrito-ui');
			contenedor.id = 'carritoCont';

			titulo.innerText = 'Carrito';

			if(this.carritoLleno){
				let boton = document.createElement('button');
				contenedor.appendChild(boton);
				boton.innerText = 'Comprar';
				boton.addEventListener(
					'click',
					function(){
						this.carritoVender();
					}.bind(this)
				);
			}
			return contenedor;
		},

		vacio: function(){
			let contenedor = document.createElement('div');
			let texto = document.createElement('p');

			contenedor.appendChild(texto);

			contenedor.classList.add('vacio-mensaje');
			if(this.carritoVendido === true){
				texto.innerText = 'Ha comprado sus productos correctamente';
			}
			else{
				texto.innerText = 'No se encontraron productos';
			}

			return contenedor;
		},

		producto: function(datos){
			let extTarjeta = document.createElement('div');
			let tarjeta = document.createElement('div');
			let texto = document.createElement('p');
			let boton = document.createElement('button');

			extTarjeta.appendChild(tarjeta);
			tarjeta.appendChild(texto);
			tarjeta.appendChild(boton);

			extTarjeta.classList.add('extTarjeta');
			tarjeta.classList.add('tarjeta');

			texto.innerText = datos.name + ' X ' + datos.cantidad;
			boton.innerText = '-';
		
			boton.addEventListener(
				'click',
				function(){
					this.productoQuitar(datos);
				}.bind(this)
			);

			return extTarjeta;
		},

		icono: function(){
			let icono = document.createElement('div');

			icono.classList.add('carrito');
			if(this.carritoMostrado){
				icono.classList.add('volver');
			}
			if(!this.carritoMostrado && this.carritoLleno){
				icono.classList.add('lleno');
			}

			icono.addEventListener(
				'click',
				function(){
					if(this.carritoMostrado){
						this.carritoUIOcultar();
					}
					else{
						this.carritoUIMostrar();
					}
				}.bind(this)
			);

			return icono;
		},
	},

	catalogo: {
		catalogo: function(productos, filtros){
			let contenedor = document.createElement('div');
			let filtrosContenedor = document.createElement('div');
			let productosContenedor = document.createElement('div');
			let filtroTitulo = document.createElement('h2');
			let filtroContenido = document.createElement('div');
			let productosTitulo = document.createElement('h2');
			let productosContenido = document.createElement('div');

			contenedor.id = 'catalogoCont';

			contenedor.appendChild(filtrosContenedor);
			contenedor.appendChild(productosContenedor);
			filtrosContenedor.appendChild(filtroTitulo);
			filtrosContenedor.appendChild(filtroContenido);
			productosContenedor.appendChild(productosTitulo);
			productosContenedor.appendChild(productosContenido);
			filtroContenido.appendChild(filtros);
			productosContenido.appendChild(productos);
		
			filtroContenido.id = 'filtroCont';
			productosContenido.id = 'productosCont';
			filtroTitulo.innerText = 'Categorías';
			productosTitulo.innerText = 'Productos';
		
			return contenedor;
		},

		vacio: function(){
			let contenedor = document.createElement('div');
			let texto = document.createElement('p');

			contenedor.appendChild(texto);

			contenedor.classList.add('vacio-mensaje');
			texto.innerText = 'No se encontraron productos';

			return contenedor;
		},

		producto: function(datos){
			let extTarjeta = document.createElement('div');
			let tarjeta = document.createElement('div');

			let descrip = document.createElement('div');
			let compra = document.createElement('div');
			let imagenContExt = document.createElement('div');
			let imagenContInt = document.createElement('div');
			let imagen = document.createElement('img');
			let nombre = document.createElement('p');
			let precio = document.createElement('div');
			let regular = document.createElement('p');
			let boton = document.createElement('button');

			extTarjeta.appendChild(tarjeta);
			tarjeta.appendChild(descrip);
			descrip.appendChild(imagenContExt);
			imagenContExt.appendChild(imagenContInt);
			imagenContInt.appendChild(imagen);
			descrip.appendChild(nombre);
			tarjeta.appendChild(compra);
			compra.appendChild(precio);
			precio.appendChild(regular);
			compra.appendChild(boton);

			extTarjeta.classList.add('extTarjeta');
			tarjeta.classList.add('tarjeta');
		
			if(datos.url_image === null || datos.url_image === undefined || datos.url_image === ''){
				let imagenRespaldo = './recursos/img/logo_bsale.svg';
				imagen.src = imagenRespaldo;
			}
			else{
				imagen.src = datos.url_image;
			}
		
			nombre.innerText = datos.name;
			regular.innerText = 'S/.' + (datos.price/100).toFixed(3);
		
			boton.addEventListener(
				'click',
				function(){
					this.carrito.productoAgregar(datos);
				}.bind(this)
			);
		
			if(datos.discount > 0){
				let rebaja = document.createElement('p');
				let oferta = document.createElement('p');
				precio.appendChild(rebaja);
				descrip.appendChild(oferta);
				oferta.classList.add('oferta');
				oferta.innerHTML = '<b>' + datos.discount + '%</b><br>Rebaja';
				regular.classList.add('tachado');
				rebaja.innerText = 'S/.' + (((100 - datos.discount) * datos.price) / 10000).toFixed(3);
			}
		
			return extTarjeta;
		},

		filtro: function(catalogo, orden){
			let extFiltro = document.createElement('div');
			let filtro = document.createElement('div');
		
			let check = document.createElement('input');
			let texto = document.createElement('label');
		
			extFiltro.appendChild(filtro);
			filtro.appendChild(check);
			filtro.appendChild(texto);
		
			extFiltro.classList.add('extFiltro');
			filtro.classList.add('filtro');
		
			check.type = 'checkbox';
			check.value = catalogo.filtrosLista[orden].id;
			if(catalogo.filtrosLista[orden].activo === true){
				check.checked = true;
			}
			texto.innerText = catalogo.filtrosLista[orden].name;
		
			check.addEventListener('change',
				function(){
					catalogo.actualizarFiltro(check);
				}.bind(this)
			);
		
			return extFiltro;
		},
	}
}

export {ui};