'use strict';

/**
Módulo que exporta la clase cookies.
@module util/cookies
*/

/**
DESCRIP.
@class
@name NOMBRE
@param {TIPO} NOMBREPARAM - DESCRIPPARAM.
@returns RETORNO
@example
EJEMPLO
*/

class cookies{
	static escribir(nombre, valor){
		document.cookie = '' + nombre + '=' + valor + '; SameSite=Lax';
	}

	static leer(nombre='') {
		let retorno = document.cookie;

		if(nombre !== ''){
			nombre = nombre + '=';
			let valor = retorno.split(';').filter(item => {
				return item.includes(nombre);
			})

			if (valor.length) {
				retorno = valor[0].substring(nombre.length, valor[0].length);
			}
			else {
				retorno = '';
			}
		}

		return retorno;
	}

	static borrar(nombre){
		document.cookie = '' + nombre + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC;'
	}
}


export {cookies}