'use strict';

/**
Módulo que exporta la clase ajax.
@module util/ajax
*/

/**
Se encarga de realizar solicitudes Ajax.
@class
@name ajax
*/
class ajax{
	/**
	Obtiene un recurso utilizando Ajax.
	@instance
	@function
	@param {object} url - URL del recurso deseado.
	@returns {object}
	*/
	static get(url){
		let retorno = new Promise(
			function(resolve, reject){
				let pedido = new XMLHttpRequest();
				pedido.open('GET', url);
				pedido.send();
				pedido.onload = function(){
					if (pedido.status >= 200 && pedido.status < 400) {
						let resp = pedido.responseText;
						resolve(resp);
					}
					else{
						let resp = pedido.status;
						reject(resp);
					}
				}
				pedido.onerror = function(){
					let resp = pedido.status;
					reject(resp);
				}
			}
		);

		return retorno;
	}
}

export {ajax};