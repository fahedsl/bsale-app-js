'use strict';

/**
Módulo que exporta la clase urls.
@module util/urls
*/

/**
DESCRIP.
@class
@name urls
*/

class urls{
	/**
	Crea un objecto URL a partir de un string.
	@function
	@param {string} url - URL con que se creará el objeto.
	@returns {object}
	*/
	static urlCrear(url){
		let retorno = new URL(url);
		return retorno;
	}

	/**
	Agrega parametros adicionales (GET) a un objeto URL.
	@function
	@param {object} url - URL con que se creará el objeto.
	@param {object} nombre - Nombre que se dará al parámetro.
	@param {object} valor - Valor que se dará al parámetro.
	@returns {object}
	*/
	static urlEntradaAParametro(url, nombre, valor){
		valor = this.textoLimpiar(valor);
		let retorno = new URL(url);
		retorno.searchParams.set(nombre, valor);

		return retorno;
	}

	/**
	Remueve caracteres no riesgosos o no permitidos de un texto.
	@function
	@param {string} valorSucio - Texto que se limpiará.
	@returns {string}
	*/
	static textoLimpiar(valorSucio){
		var elemento = document.createElement('div');
		elemento.innerText = valorSucio;
		var valorLimpio = elemento.innerHTML;
		return valorLimpio;
	}
}

export {urls};