# **app-js**

## Ejercicio Completo
Construir una tienda online que despliegue productos agrupados por la categoría a
la que pertenecen, generando por separado backend (API REST) y frontend (aplicación
que la consuma) y utilizando la base de datos que se disponibiliza para su desarrollo.

## Aplicacion cliente
- Crear una tienda online que despliegue productos agrupados por la categoría a la que pertenecen.
- Agregar un buscador, implementado a nivel de servidor.
- Desarrollada con vanilla javascript.
- Finalmente, disponibilizar la aplicación y el repositorio con el código en un hosting.

## Ejecución
Se creó la aplicación cliente y se puso a disponibilidad en un [VPS no administrado](https://algorit.io/etc/bsale/app-js).